/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package BGServer;

import java.net.Socket;
import java.util.Calendar;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;


/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class ClientInfo
{
   
    public Socket client;
    public java.sql.Timestamp ConnectTimestamp;
    
    // Do not access bufferOut directly.  It must be accessed through the
    // synchronized push and pop in order to stay thread-safe.
    // TODO: May be upgraded to a PriorityQueue in the future.
    public BlockingQueue<String> bufferOut;
    public String username = "NONAME";
    public Thread inputThread;
    public Thread outputThread;
    
    public ClientInfo( Thread _inputThread, Socket _client )
    {
        this.bufferOut = new LinkedBlockingQueue<String>();
        this.client = _client;
        this.inputThread = _inputThread;
        Calendar calendar = Calendar.getInstance();
        java.sql.Timestamp now = new java.sql.Timestamp( 
                calendar.getTime().getTime() );
        
        this.ConnectTimestamp = now;

    }
    
    
    public synchronized void setOutputThread( Thread _outputThread )
    {
        this.outputThread = _outputThread;
        
    }
    
    
    /**
     * Push a msg to the client's outgoing buffer.  All msgs in the buffer get
     * sent out by that client's outgoing socket. This method MUST be
     * synchronized to prevent multiple threads from adding data to this
     * buffer at once (data corruption will occur).
     * @param msg 
     */
    public void bufferPush( String msg )
    {
        synchronized ( this.bufferOut )
        {
            this.bufferOut.add( msg );
            System.out.printf( "Buffer Size: %s%n", this.bufferOut.size() );
            this.bufferOut.notify();
            
        }
    }
    
    
    /**
     * Pop a msg from the client's outgoing buffer so that it can be sent out
     * on the outgoing socket connection.  This method MUST be synchronized
     * to prevent multiple threads from popping data at once.
     */
    public String bufferPop() throws InterruptedException
    {
        return this.bufferOut.take();
    
    }
    
    
    
    
    
    
    
    
}

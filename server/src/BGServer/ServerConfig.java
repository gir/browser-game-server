/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package BGServer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;


/**
 * Loads config keys from a file (if present) and validates each specified
 * key.  Invalid/Missing keys are ignored default values are used instead.
 * 
 * @author Kayvan Boudai and Michael Brich
 */
public class ServerConfig 
{
    
    
    public int port = 8181;
    public String configFileDir = "./";
    public String configFileName = "config.properties";
    
    
    public ServerConfig( )
    {
        // TODO: Command line argument parser to allow config filename
        // and dir to be specified.
        String configFilePath = this.configFileDir + this.configFileDir;
        System.out.printf( "[BGServer] Looking for config file: %s%n", 
                          configFilePath );
        
        Properties properties = new Properties();
        boolean UseConfigFile = false;
        try 
        {
            
            FileInputStream fileStream = new FileInputStream( configFilePath );
            properties.load( fileStream );
            fileStream.close();
            UseConfigFile = true;
            
        }
        catch ( FileNotFoundException e )
        {
            System.err.println( "[BGServer] Config file does not exist in the "
                    + "server directory. ");
            
        } 
        catch ( SecurityException e )
        {
           System.err.println( "[BGServer] Config file found, but the server "
                   + "does not have permission to read it." );
        
        }
        catch ( IOException e )
        {
            e.printStackTrace();
            
        }
        
        
        if ( UseConfigFile == true )
        {
            System.out.println( "[BGServer] Config file found! Loading keys.." );
            
            
            if ( properties.getProperty( "port" ) != null )
            {
                this.setPort( properties.getProperty( "port" ) );
                
            }
            
            
        }
        
    } // ef
    
    
    /**
     * Set the server's listening port. If there are any problems parsing 
     * the string to an int, the user is warned and the default port is used
     * instead.
     * @param portNum
     */
    public void setPort( String portNum )
    {
        
        try 
        {
            int portInt = Integer.parseInt( portNum );
            this.setPort( portInt );
            
        }
        catch ( NumberFormatException e )
        {
            System.err.println( "[BGServer] Invalid port value specified in "
                    + "the server settings file.  This value could not be"
                    + "interpreted as a valid number." );
            
        }
    } // ef
    
    
    
    /**
     * Set the server's listening port. Attempts to validate and warn the user
     * if the port number is invalid.  Default port number is used if this
     * fails.
     * @param portNum
     */
    public void setPort( int portNum )
    {
        if ( portNum > 65535 || portNum < 1 )
        {
            System.err.println( "[BGServer] Invalid port number specified in"
                    + "the server settings file.  Please select a port number"
                    + "between 1 and 65535" );
            
        }
        else
        {
            this.port = portNum;  
            
        }
    } // ef
}

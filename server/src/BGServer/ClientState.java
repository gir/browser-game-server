
package BGServer;

// Progression of States
// 1. CONNECT
// 2. HANDSHAKE
// 3. AUTHENTICATE
// 4. DISCONNECT
public enum ClientState { CONNECT, HANDSHAKE, AUTHENTICATE, DISCONNECT }
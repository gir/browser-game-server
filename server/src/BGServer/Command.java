/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BGServer;

/**
 * Specifies the required methods to create a new command.
 * @author Kayvan Boudai and Michael Brich
 */
public interface Command 
{
    
    void run();
    int getArgCount();
    
}

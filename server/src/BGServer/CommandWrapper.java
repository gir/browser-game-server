/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package BGServer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author Kayvan Boudai and Michael Brich
 */
public class CommandWrapper 
{
    
    public static HashMap<String,Command> commandMap;
    
    public CommandWrapper()
    {

        
    }
    
    
    /**
     * Initializes the CommandWrapper. MUST be called before any methods
     * are used.
     * 
     */
    public static void init()
    {
        System.out.println( "CommandWrapper init" );
        commandMap = new HashMap<String,Command>();
        
        CommandList defaultList = new DefaultCommandList();
        addCommandList( defaultList.get() );      
        
    }
    
    /**
     * Takes a commandList object and adds the list of commands to commandMap.
     * Duplicate keys will result in an exception thrown.
     * @param commandList   The HashMap<String,Command> object that should be
     * added to the current commandMap.
     */
    public static void addCommandList( HashMap<String,Command> commandList )
    {
        System.out.println( "[BGServer] Adding commands..." );
        Iterator iter = commandList.keySet().iterator();
        
        while ( iter.hasNext() )
        {
            try 
            {
                String key = (String) iter.next();

                if ( commandMap.containsKey (key) ) 
                {
                    // TODO: Give this a real exception msg.
                    throw new CommandExistsException( "Something" );

                }            
                commandMap.put( key, (Command)commandList.get( key ) );
                
            }
            catch ( CommandExistsException e )
            {
                e.printStackTrace();
            
            }
        }
    } // ef
    
    
    /**
     * A
     * @param commandName
     * @return
     */
    public static boolean hasCommand( String commandName )
    {
        if ( commandMap.containsKey( commandName ) )
        {
            return true;
    
        }
        else
        {
            return false;
            
        }
    } // ef
    
    
    
    public static boolean doCommand( String commandName )
    {
        System.out.printf( "doCommand: %s%n", commandName );
        if ( hasCommand( commandName ) )
        {
            Command command = (Command) commandMap.get( commandName );
            switch (commandName)
            {
            	case "MTC":
            		command.run();
            		break;	
            		
            	default:
            		command.run();
            }
            return true;
            
        }
        else
        {
            System.out.println( "No such command" );
            return false;
            
        }
    }

}

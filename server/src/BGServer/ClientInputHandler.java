/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package BGServer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;


public class ClientInputHandler implements Runnable 
{
    
    Socket client;
    ClientState clientState;
    DataOutputStream toClient;
    DataInputStream fromClient;
    WebsocketProtocol protocol;
    StringTokenizer tokenizer;
    CommandWrapper commandWrapper;
    int clientId;
    
    public ClientInputHandler( int _clientId, Socket Client, 
            WebsocketProtocol _protocol ) 
    {
        this.client = Client;
        this.clientId = _clientId;
        this.clientState = ClientState.CONNECT;
        
        // Protocol set to 13 for now, should be adjustable in the future
        // if we need to implement new handling but also maintain legacy compat.
        this.protocol = _protocol;
        
        
        try 
        { 
            this.toClient = new DataOutputStream( this.client.getOutputStream() );
            this.fromClient = new DataInputStream( this.client.getInputStream() );
            
        } 
        catch ( IOException e ) 
        {
            // TODO: Handle exception
            System.err.printf( "IOE: %s%n", e.getMessage() );
            e.printStackTrace();
            
        }
    } // ef
    
    
    
    public void run() 
    {
        try 
        {
            
            Scanner scanner = new Scanner( client.getInputStream() );
            
            this.protocol.doHandshake( scanner, this.toClient );
            
            
            ClientOutputHandler clientOutputHandler =
                new ClientOutputHandler( this.clientId, this.toClient, 
                    this.protocol );
                
           Thread outputThread = new Thread( clientOutputHandler );
           outputThread.start();
           ClientList.setOutputThread( this.clientId, outputThread );
            for ( ;; ) 
            {
            	//TODO: add actual break out of loop and disconnects sequence
                if ( this.clientState == ClientState.DISCONNECT ) return;
                this.processIncoming();
                
            }
            
            //TODO: add disconnect sequence here (maybe send close message to client then delete from client list
            
        } 
        catch ( IOException e )//| InterruptedException e ) 
        {
            //: Handle exn or throw it further up the stack.
            //Logger.getLogger(client_socket.class.getName()).log(Level.SEVERE, null, ex);
            e.printStackTrace();
    
        }
    } // ef
    
    
    /**
     * Sends a message to the client. The message is passed through the
     * protocol specific sendMessage function and then sent out through the
     * socket.
     * @param msg 
     */
    public void sendMessage( String msg )
    {
        this.protocol.sendMessage( this.toClient, msg );
        
    }
  
    
     /**
     * Parses incoming messages into a command and a set of arguments that can
     * be passed to the command map.  Note that the use of a tokenizer is an 
     * intentional design decision.  Msg parsing is potentially a bottleneck, 
     * thus speed is critical.  We prefer the speed of the tokenizer because we 
     * do not require the power or flexibility offered by string.split().
     */   
    public void processIncoming()
    {
        // TODO: Change string delim to char
        // TODO: reset tokenizer instead of making a new one for each msg.
        try
        {
                String msg = this.protocol.receiveMessage( this.fromClient );
                System.out.println( "Read Message: " + msg );
                
                
                ArrayList<String> tokens = new ArrayList<String>();
                this.tokenizer = new StringTokenizer( msg, "\037" );
        
        
                while ( this.tokenizer.hasMoreTokens() )
                {
                    tokens.add( this.tokenizer.nextToken() );

                }
               
                // We need a command to pass into the CommandWrapper.
                /*TODO: fix blockage: by sending the info to the command 
                 * Wrapper class(another thread) that checks and instantiates 
                 * new commands to unblock input
                 */
                if ( tokens.size() > 0 )
                {
                    if ( !this.commandWrapper.doCommand( tokens.get( 0 ) ) )
                    {
                    	
                        // Invalid command
                        
                    }
                    else
                    {
                        System.out.printf( "Command done:%n" );
                        
                    }      
            } 
        }
        catch ( IOException e )
        {
            // Something bad happened, close the connection.
            // TODO: Log the error also.
        	e.printStackTrace();
            this.close();
            
        }
        
    }
    
    
    
    /**
     * Close the client's connection.  Handles changing the clientState,
     * sends a protocol-specific connection closing message, and finally closes
     * the actual socket connection.
     */
    public void close() 
    {
        
        try 
        {
            this.clientState = ClientState.DISCONNECT;
            
            // Let the protocol perform any cleanup or send any closing
            // notifications to the client.
            this.protocol.close(toClient);
            
            //WebsocketProtocol13.clientList.remove(this);
            
            this.client.close();
            System.out.println( "Client socket being closed" );
            
        } 
        catch ( IOException e ) 
        {
            System.err.printf( "IOE: %s%n", e.getMessage() );
            e.printStackTrace();
            
        }
    } // ef
    
    
    //TODO: what happens when the client closes without handshake?
    public void errClose() 
    {
        
        try 
        {
            this.clientState = ClientState.DISCONNECT;
            
            
            // Client closed without handshake?
            
           // WebsocketProtocol13.clientList.remove(this);
            
            this.client.close();
            System.out.println( "Client socket being closed" );
            
        } 
        catch ( IOException e ) 
        {
            System.err.printf( "IOE: %s%n", e.getMessage() );
            e.printStackTrace();
            
        }
    } // ef
    
}
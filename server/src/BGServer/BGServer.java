/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package BGServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;



public class BGServer 
{
	//boolean exit = false;
	
    public static void main( String[] args )
    {
        System.out.println( "[BGServer] Initializing..." );
        
        // The config object reads config.properties in the server dir and uses
        // any config values it finds. Default values are used when an expected
        // field does not exist.  Default values are also used if the config
        // file does not exist.
        ServerConfig serverConfig = new ServerConfig();
        
        //TODO: Use serverconfig to add what protocol that is being used or not
        WebsocketProtocol protocol = new WebsocketProtocol13();
        
        try 
        {
            ServerSocket Server = new ServerSocket( serverConfig.port );
            System.out.printf( "[BGServer] Now listening on port "
                    + "%s%n", serverConfig.port );
            CommandWrapper.init();
            
            for ( ;; ) 
            {
                Socket client = Server.accept();
                
                // We have a lot of small packets that may be delayed or
                // held back due to Nagle's algorithm. Disable it.
                client.setTcpNoDelay( true );
               
                
                int nextClientId = ClientList.getNextClientId();
                
                ClientInputHandler clientInputHandler = 
                        new ClientInputHandler( nextClientId, client, protocol );
                

                Thread inputThread = new Thread( clientInputHandler );
                inputThread.start();

                // Register our client in the connection list.
                // We store references to the socket and to both I/O threads so
                // that we can shut down them down when we're done.
                ClientList.register( inputThread, client );
                
                
                //if(exit) break;
           }
            
            //Server.close();
            
        } 
        catch ( UnknownHostException e ) 
        {
            System.err.println( e.getMessage() );
            System.exit( ReturnCodes.FAILURE );
            
        } 
        catch ( IOException e ) 
        {
            System.err.println( e.getMessage() );
            e.printStackTrace();
            System.exit( ReturnCodes.FAILURE );
            
        }
    } // ef

    
}

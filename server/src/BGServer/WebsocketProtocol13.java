/*
 * Copyright (C) 2013 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package BGServer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Scanner;
import org.apache.commons.codec.binary.Base64;


public class WebsocketProtocol13 implements WebsocketProtocol 
{
	
    static final byte OPCODE_BITMASK   = 0x0F;
    static final byte OPCODE_CLOSE     = 0x01;
    static final byte OPCODE_PING      = 0x02;
    static final byte OPCODE_PONG      = 0x03;
    static final byte LENGTH_MASK      = 0x7F;
    final String protocolStr = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
    
    // Purpose: Constructs the key for Sec-WebSocket-Accept
    // Concat the protocol string onto the client's provided key.  Get the sha1
    // hash for this string, and finally base64 encode that hash to get our
    // "encoded key".  The encoded key is returned to the client during the
    // handshake.  If this key is incorrect, the client aborts their connection
    // request.
    public String getEncodedKey( String key ) 
    {
        
        try 
        {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            
            byte[] sha1 = md.digest( ( key + this.protocolStr ).getBytes() );
            String encodedKey = new String( Base64.encodeBase64( sha1 ) );

            return encodedKey;
            
        } 
        catch ( NoSuchAlgorithmException e ) 
        {
            
            System.err.println( "Exception: " + e.getMessage() );
            return "INVALID";
            
        }
    } // ef 
    
    
    
    public void sendFrame()
    {
        
    }
    
    
    public void sendMessage( DataOutputStream toClient, String msg ) 
    {
        
        try 
        { 
            // TODO: Proper op-code handling.
            // TODO: Proper length handling.
            toClient.write( 0x81 );
            
            if ( msg.length() <= 125 )
            {
                toClient.write( msg.length() );
            
            }
            
            toClient.writeBytes( msg );
            toClient.flush();
            
            
        } 
        catch ( IOException e ) 
        {
            // TODO: Exception handling.
            System.err.printf( "IOE: %s%n", e.getMessage() );
            e.printStackTrace();
            
        }
    } // ef
    
    
    /*
    public void broadcastMessage( int clientID, String msg ) 
    {
    	System.out.println("ClientID: " + clientID + " Brodcasting message: " + msg);
    	
        for(ClientHandler client : clientList)
        {
        	if(client.clientID == clientID )
        	{
        		System.out.println("Skipped ClientID: " + client.clientID);
        		continue;
        	}
        	
        	DataOutputStream toClient = client.toClient;
	        try 
	        { 
	            // TODO: Proper op-code handling.
	            // TODO: Proper length handling.
	        	toClient.write( 0x81 );
	            
	            if ( msg.length() <= 125 )
	            {
	                toClient.write( msg.length() );
	            }
	            
	            toClient.writeBytes( msg );
	            toClient.flush();
	            
	        } 
	        catch ( IOException e ) 
	        {
	            // TODO: Exception handling.
	            System.err.printf( "IOE: %s%n", e.getMessage() );
	            e.printStackTrace();
	            client.errClose();
	            
	        }
        }
    } // ef
    */
   
    
    public String receiveMessage( DataInputStream fromClient ) throws IOException
    {
        
        byte[] b = new byte[2];
        int msgLength = 0;
        if ( fromClient.read( b ) != -1 )
        {
            System.out.println( "Reading Message" );
            
            //read in dataLength code
            byte dataLength = b[1];
            byte op = (byte) 127;
            
            //remove mask bit from datalength
            dataLength = (byte)(dataLength & op );
            
            //get message lenth through byte to in conversion
            msgLength = (int) readLength( dataLength, fromClient );
            
            byte[] masks = new byte[4];
        
            //get mask code
            fromClient.read(masks);

            byte[] payload = new byte[msgLength];
            
            //get payload data
            fromClient.read( payload );
        
        
            byte[] msg = new byte[msgLength];
            
            //convert data into UTF-8 data
            for ( int i = 0; i < msgLength; ++i )
            {
            	msg[i] = (byte) ( payload[i] ^ masks[i % 4] );
            
            
            }
        
            //convert into string then return
            return (new String( msg, "UTF-8" ) );
        
        }
        else
        {
            //TODO: proper fail procedure
            return null;
            
        }
        
        
    }//  ef
    
    
    static long readLength( int firstByte, InputStream istr ) throws IOException 
    {
        long length = firstByte & 0x7F;
        switch ( firstByte )
        {
            // 16 bit length
            case 126:
            length = (readByte(istr) << 8) | readByte(istr);
                break;
            
            // 63-bit length
            case 127:
                length = (readByte(istr) << 54) | (readByte(istr) << 48)
                    | (readByte(istr) << 40) | (readByte(istr) << 32)
                    | (readByte(istr) << 24) | (readByte(istr) << 16)
                    | (readByte(istr) << 8) | readByte(istr);
                break;
        }

            return length;
        
    } // ef

    
    
    static int readByte(InputStream istr) throws IOException 
    {
        int val = istr.read();
        if ( val < 0 ) throw new IOException("EOF on read");
       
        return val;
        
    } // ef
    
    
    public boolean doHandshake( Scanner scanner, DataOutputStream toClient ) 
    {
        
        boolean IsKeySet = false;
        boolean IsVerSet = false;
        String EncodedKey = "";
        
        String line = "";
        
        int counter = 0;
        while ( scanner.hasNextLine() && ( line = scanner.nextLine() ) != null )
        {
            String[] tokens = line.split( ": " );
            
            if ( tokens[0].equals( "Sec-WebSocket-Key" ) )
            {
                IsKeySet = true;
                if ( tokens.length >= 2 )
                {
                    System.out.printf( "Client Key: %s%n", tokens[1] );
                    EncodedKey = this.getEncodedKey(tokens[1] );
                    
                }
                else
                {
                    // TODO: Do an exception here.
                    System.err.println( "No key" );
                    
                }
            }
            else if ( tokens[0].equals( "Sec-WebSocket-Version" ) ) 
            {
                IsVerSet = true;
                // TODO: Do something with the version here.
            }
            
            if ( IsKeySet && IsVerSet )
            {
                // Be EXTREMELY careful that the header ends with \r\n\r\n
                // and nothing else.  Any data after the double \r\n will
                // be processed as part of the next message received by the
                // client and will likely cause the connection to be
                // terminated.
                String msg = "HTTP/1.1 101 Switching Protocols\r\n";
                msg += "Upgrade: websocket\r\n";
                msg += "Connection: Upgrade\r\n";
                msg += "Sec-WebSocket-Accept: " + EncodedKey;
                msg += "\r\n\r\n";                
                
                try { 
                    
                    toClient.writeBytes( msg );
                    toClient.flush();
                    System.out.println( "after" );
                    
                    System.out.println( "Server > Client: " + msg );
                    return true;
                    
                }
                catch ( IOException e )
                {
                    System.err.printf( "Handshake IOE: %s%n", e.getMessage() );
                    e.printStackTrace();
                    return false;
                    
                }
            }
            
            ++counter;
            
            // Handshaking should have been completed long before the counter
            // ever reaches 20.  It shouldn't go past 12 or 13 headers, based
            // on the current spec.  Bail out if we're receiving garbage data.
            if ( counter >= 20 ) return false;
            
        }

        return false;
        
    } // ef    
    
    
    /**
     * Called by the client when the server intends to close the connection.
     * Only handles any closing messages required by this version of the 
     * protocol. Closing the actual socket connection must be handled elsewhere.
     */
    public void close( DataOutputStream toClient )
    {
    	String msg = "close";
        try 
        { 
            // TODO: Proper op-code handling.
            // TODO: Proper length handling.
            toClient.write( 0x81 );
            
            if ( msg.length() <= 125 )
            {
                toClient.write( msg.length() );
            
            }
            
            toClient.writeBytes( msg );
            toClient.flush();
            
            
        } 
        catch ( IOException e ) 
        {
            // TODO: Exception handling.
            System.err.printf( "IOE: %s%n", e.getMessage() );
            e.printStackTrace();
            
        }
        
    }
    
}